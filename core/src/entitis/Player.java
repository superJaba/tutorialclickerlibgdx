package entitis;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by Karamba on 2017-08-19.
 */

public class Player extends Image {

    private final static int WIDTH = 77;
    private final static int HEIGHT = 152;

    private final static int STARTING_X = 200;
    private final static int STARTING_Y = 300;
    private static final int Y_MOVE_AMOUNT = 10;
    public static final float MOVE_TIME_DURATION = 0.3f;
    public static final int Y_GROWE_AMOUNT = 20;
    public static final float GROWE_TIME_DURATION = 0.2f;

    public Player() {
        super(new Texture("badlogic.jpg"));

        this.setOrigin(WIDTH / 2, HEIGHT / 2);
        this.setSize(WIDTH, HEIGHT);

        //starting position
        this.setPosition(STARTING_X, STARTING_Y);
    }

    public void reactOnClick() {
        int xMoveAmount = MathUtils.random(-130, 130);
        Action moveAction = Actions.sequence(
                Actions.moveBy(xMoveAmount, Y_MOVE_AMOUNT, MOVE_TIME_DURATION, Interpolation.circleOut),
                Actions.moveBy(-xMoveAmount, -Y_MOVE_AMOUNT, MOVE_TIME_DURATION, Interpolation.circle)
        );

        int xGrowAmount = MathUtils.random(-40, 120);
        Action groweAction = Actions.sequence(
                Actions.sizeBy(xGrowAmount, Y_GROWE_AMOUNT, GROWE_TIME_DURATION, Interpolation.elasticOut),
                Actions.sizeBy(-xGrowAmount, -Y_GROWE_AMOUNT, GROWE_TIME_DURATION, Interpolation.circleIn)
        );

        this.addAction(moveAction);
        this.addAction(groweAction);

        if (this.getHeight() > 170) {
            this.addAction(Actions.rotateBy(MathUtils.randomSign()*360,0.3f,Interpolation.circle));
        }
    }
}
