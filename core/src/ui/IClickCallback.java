package ui;

/**
 * Created by Karamba on 2017-08-28.
 */

public interface IClickCallback {
    void onClick();
}
