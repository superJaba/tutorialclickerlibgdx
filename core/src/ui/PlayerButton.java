package ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Karamba on 2017-08-28
 */

public class PlayerButton extends Button {

    public PlayerButton(final IClickCallback callback) {

        super(new ButtonStyle());

        init(callback);
    }

    public void init(final IClickCallback callback) {
        this.setSize(460, 360);
        this.setX(10);
        this.setY(170);
//        this.setDebug(true);//tymczasowo, aby widziec rozmiar buttona

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                callback.onClick();

                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }
}
