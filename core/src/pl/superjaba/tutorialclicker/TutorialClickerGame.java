package pl.superjaba.tutorialclicker;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import screens.SplashScreen;

public class TutorialClickerGame extends Game {

    public final static String GAME_PREFS = "pl.superjaba.preclicker.prefs";
    public final static String GAME_SCORE = "pl.superjaba.preclicker.prefs.score";
    public final static String GAME_NAME = "Tutorial Clicker";

    public final static int WIDTH = 480;
    public final static int HEIGHT = 700;

    private Preferences prefs;

    private boolean paused;
    private int points;


    @Override
    public void create() {
        init();
        this.setScreen(new SplashScreen(this));
    }

    public void init() {
        prefs = Gdx.app.getPreferences(GAME_PREFS);
        loadScore();
    }

    private void loadScore() {
        points = prefs.getInteger(GAME_SCORE);
    }

    public void addPoint() {
        points++;
        updateSavedScoreInPrefs();
    }

    public void resetGameScore() {
        points = 0;
        updateSavedScoreInPrefs();
    }

    public void updateSavedScoreInPrefs() {
        prefs.putInteger(GAME_SCORE, points);
        prefs.flush();
    }

    /**
     * getery i setery
     */
    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public int getPoints() {
        return points;
    }


}
