package screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Timer;

import pl.superjaba.tutorialclicker.TutorialClickerGame;

/**
 * Created by Karamba on 2017-08-19.
 */

public class SplashScreen extends AbstractScreen {

    private Texture spashImg;

    public SplashScreen(final TutorialClickerGame game) {
        super(game);

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                game.setScreen(new GameplayScreen(game));
            }
        }, 1);
    }

    @Override
    protected void init() {
        //TODO implement better asets loading when game grows
        spashImg = new Texture("badlogic.jpg");
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        spriteBatch.begin();
        spriteBatch.draw(spashImg, 0, 0);
        spriteBatch.end();
    }
}
